/** These are some calculations functions
 * (Because we don’t like `Math`)
 */

const add = a => b => a + b

const subtract = a => b => b - a

const multiply = a => b => [...Array(a).keys()].reduce(add(b), 0)

const equals = a => b => a && a === b

const isBiggerThan = a => b => b > a
const isSmallerThan = a => b => b < a

const isNegativeNumber = a => isNumeric(a) && a < 0

const absolute = a => isNegativeNumber(a) ? -a : a

const power = a => b => new Array(a).reduce(multiply(b), 1)

function isNumeric(a){
  return !isNaN(a)
}
const absoluteValue = a => isNegative(a) ? -a : a

const distance = a => b => absoluteValue(a - b)

module.exports = {
  add, multiply, equals, isBiggerThan, isSmallerThan, isNegativeNumber, absolute, power
}
